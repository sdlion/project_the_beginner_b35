<?php
namespace App\Booking;


use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class Booking extends DB{
    public $id;
    public $patient_name;
    public $email;
    public $catagory;
    public $doctor_name;
    public $date;
    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('patient_name',$data)){
            $this->patient_name=$data['patient_name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('catagory',$data)){
            $this->catagory=$data['catagory'];
        }
        if(array_key_exists('doctor_name',$data)){
            $this->doctor_name=$data['doctor_name'];
        }
        if(array_key_exists('date',$data)){
            $this->date=$data['date'];
        }
        return $this;
    }
        public function store() {

        $arrData =array($this->patient_name,$this->email,$this->catagory,$this->doctor_name,$this->date);

        $sql="INSERT INTO booking(patient_name,email,catagory,doctor_name,date) VALUES (?,?,?,?,?)";

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute($arrData);
            if ($result) {
                Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
                return Utility::redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
                return Utility::redirect($_SERVER['HTTP_REFERER']);
            }
    }
    public function index($fetchMode='ASSOC')
    {

        $STH = $this->conn->query('SELECT * from booking');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }
    // end of index();
    public function view(){
        $query="SELECT * FROM `booking` WHERE `booking`.`email` =:email";
        $result=$this->conn->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $row=$result->fetch(PDO::FETCH_OBJ);
        return $row;
    }
        // end of view()
}

