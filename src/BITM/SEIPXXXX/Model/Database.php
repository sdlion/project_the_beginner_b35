<?php
namespace App\Model;

use PDO;
use PDOException;


class Database{
    public $conn;

    public $host="localhost";
    public $dbname="final_project_beg";
    public $username="root";
    public $password="";

    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
          //  $this->conn = new PDO("mysql:host=;dbname=final_project_beg", $this->username, $this->password);
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password);

        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
}
