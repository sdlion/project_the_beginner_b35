<?php
namespace App\Admin;


use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class Admin extends DB{
    public $id;
    public $doctor_name;
    public $email;
    public $doctor_details;
    public $catagory;
    public $phone;
    public $address;
    public $time_duration;
    public $image;
    public $appointTimeStartHour;
    public $appointTimeStartMunute;
    public $appointTimeEndHour;
    public $appointTimeEndMunute;
    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('doctor_name',$data)){
            $this->doctor_name=$data['doctor_name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('doctor_details',$data)){
            $this->doctor_details=$data['doctor_details'];
        }
        if(array_key_exists('catagory',$data)){
            $this->catagory=$data['catagory'];
        }
        if(array_key_exists('phone',$data)){
            $this->phone=$data['phone'];
        }
        if(array_key_exists('address',$data)){
            $this->address=$data['address'];
        }
        if(array_key_exists('time_duration',$data)){
            $this->time_duration=$data['time_duration'];
        }
        
        if(array_key_exists('image',$data)){
            $this->image=$data['image'];
        }
        if(array_key_exists('appointTimeStartHour',$data)){
            $this->appointTimeStartHour=$data['appointTimeStartHour'];
        }
        if(array_key_exists('appointTimeStartMunute',$data)){
            $this->appointTimeStartMunute=$data['appointTimeStartMunute'];
        }
        if(array_key_exists('appointTimeEndHour',$data)){
            $this->appointTimeEndHour=$data['appointTimeEndHour'];
        }
        if(array_key_exists('appointTimeEndMunute',$data)){
            $this->appointTimeEndMunute=$data['appointTimeEndMunute'];
        }

        return $this;
    }
    public function store() {

        $arrData =array($this->doctor_name,$this->email,$this->doctor_details,$this->catagory,$this->phone,$this->address,$this->time_duration,$this->image,$this->appointTimeStartHour,$this->appointTimeStartMunute,$this->appointTimeEndHour,$this->appointTimeEndMunute);

        $sql="INSERT INTO doctor(doctor_name,email,doctor_details,catagory,phone,address,time_duration,image,appointTimeStartHour,appointTimeStartMunute,appointTimeEndHour,appointTimeEndMunute) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute($arrData);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
        else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }
    public function index($fetchMode='ASSOC')
    {

        $STH = $this->conn->query('SELECT * from doctor');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }
    // end of index();
    public function view($fetchMode='ASSOC')
    {

        $STH = $this->conn->query('SELECT * from doctor where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    // end of view()
    public function update()
    {
        if(!empty($this->image)) {
            $arrData = array($this->doctor_name, $this->email, $this->doctor_details, $this->catagory, $this->phone, $this->address,$this->time_duration, $this->image,$this->appointTimeStartHour,$this->appointTimeStartMunute,$this->appointTimeEndHour,$this->appointTimeEndMunute);
            $sql = "UPDATE  doctor set doctor_name =?,email =?, doctor_details=?,catagory =?,phone =?,address =?,time_duration=?,image=?,appointTimeStartHour =?,appointTimeStartMunute =?,appointTimeEndHour =?,appointTimeEndMunute =? where id=" . $this->id;
        }
        else {
            $arrData = array($this->doctor_name, $this->email, $this->password, $this->catagory, $this->phone, $this->address, $this->time_duration);
            $sql = "UPDATE  doctor set doctor_name =?,email =?,doctor_details=?,catagory =?,phone =?,address =?,time_duration=? where id=" . $this->id;

        }
        $STH = $this->conn->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }
    public function delete()
    {
        $sql="Delete from doctor where id=".$this->id;

        $STH = $this->conn->prepare($sql);
        $result=$STH->execute();
        if($result)
            Message::message("Success!Data has been deleted successfully");
        else
            Message::message("Failed!Data has been deleted successfully");

        Utility::redirect('index.php');
    }


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byCatagory']) )  $sql = "SELECT * FROM `doctor` AND (`doctor_name` LIKE '%".$requestArray['search']."%' OR `catagory` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byCatagory']) ) $sql = "SELECT * FROM `doctor` AND `doctor_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byCatagory']) )  $sql = "SELECT * FROM `doctor`  AND `catagory` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `doctor`";

        $STH = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->doctor_name);
        }

        $STH = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->catagory);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->catagory);
        }
        $STH = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->catagory);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);
    }
}