<?php
session_start();
require_once("../../vendor/autoload.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Appointment management system</title>


    <!-- Bootstrap Core CSS -->
    <link href="../../resource/assets3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../resource/assets3/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom Theme files -->
    <link href="../../resource/assets3/css/style.css" rel='stylesheet' type='text/css' />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../resource/assets3/js/jquery.min.js"></script>

    <!-- Owl Carousel Assets -->
    <link href="../../resource/assets3/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../../resource/assets3/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../resource/assets3/js/html5shiv.js"></script>
    <script src="../../resource/assets3/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- /////////////////////////////////////////Top -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="logo"><img src="../../resource/assets3/images/logo.png" alt="Third slide"></a>  <!... logo of health care...!>
            </div>
            <div class="col-md-6 text-right">
                <span>Information Service:</span></br>
                <strong class="contact-phone"><i class="fa fa-phone"></i>+00088061927</strong>
            </div>
        </div>
    </div>
</header>
<!-- Header -->

<!-- /////////////////////////////////////////Navigation -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> <!... slider..!>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>

        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="dropdown-toggle"   href="index.php">Home</a>
                </li>
                <li>
                    <a class="dropdown-toggle"   href="User/About.php">About</a>
                </li>
                
                <li>
                    <a class="dropdown-toggle"  href="User/Blog.php">Blogs</a>
                </li>
                <li>
                    <a class="dropdown-toggle"  href="User/phone.php">Contacts</a>
                </li>
                <?php if (isset($_SESSION['email']) && $_SESSION['email'] != null) { ?>
                    <li><a href="User/Authentication/logout.php" class="page-scroll">Logout</a> </li>
                <?php } else { ?>
                    <li>
                        <a class="dropdown-toggle"  href="User/Profile/login_page.php">Login</a>
                    </li>
                <?php } ?>
            </ul>
                </div>
        </div>
    </div>
</nav>

<div class="slide">
    <div class="container">
        <div class="row">
            <!-- Carousel -->
            <div id="carousel-example-generic" class="carousel slide box-shadow" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="../../resource/assets3/images/b3.jpg" alt="First slide">   <!.... first slider..!>
                        <!-- Static Header -->
                    </div>
                    <div class="item">
                        <img src="../../resource/assets3/images/b4.jpg" alt="Second slide">
                        <!-- Static Header -->
                    </div>

                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div><!-- /carousel -->
        </div>
    </div>
</div>

<a id='backTop'>Back To Top</a>
<!-- /Back To Top -->

<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page">



    <!-- ////////////Content Box 02 -->
    <section class="box-content box-2 container box-style-1 box-shadow">
        <div class="row">
            <div class="col-md-12">
                <h3>Offering a wide range of Appointment services</h3>
                <p>Our fully computerised laboratory group offers a high quality, cost-effective service and clinical expertise to local general practitioners and other trusts, hospitals and healthcare providers. Our innovative laboratory services enhance patient health.</p>

            </div>
        </div>
    </section>

    

<footer>
    <div class="wrap-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-footer footer-1">
                    <div class="footer-heading"><h4>Partners</h4></div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../resource/assets3/images/22.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../resource/assets3/images/23.jpg" /></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../resource/assets3/images/24.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../resource/assets3/images/18.jpg" /></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../resource/assets3/images/19.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../resource/assets3/images/20.jpg" /></a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-footer footer-2">
                    <div class="footer-heading"><h4>About Us</h4></div>
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad</p>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-3">
                    <div class="footer-heading"><h4>Follow us</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a></li>
                            <li><a href="#"><i class="fa fa-rss"></i> RSS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-4">
                    <div class="footer-heading"><h4>Navigation</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="User/Authentication/login_view.php"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="User/About.php"><i class="fa fa-users"></i> About</a></li>
                            <li><a href="User/Doctor.php"><i class="fa fa-ambulance"></i> Services</a></li>
                            <li><a href="Admin/login_page.php"><i class="fa fa-folder-open-o"></i> Admin</a></li>
                            <li><a href="User/phone.php"><i class="fa fa-envelope-o"></i> Contacts</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="coppy-right">
    <div class="wrap-footer">
        <div class="clearfix">
            <div class="col-md-6 col-md-offset-3">
                <p>Copyright @ 2016 The Begginners</p>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->

<!-- Core JavaScript Files -->
<script src="../../resource/assets3/js/bootstrap.min.js"></script>
<script src="../../resource/assets3/js/jquery.backTop.min.js"></script>
<script>
    $(document).ready( function() {
        $('#backTop').backTop({
            'position' : 1200,
            'speed' : 500,
            'color' : 'red',
        });
    });
</script>

<!-- carousel -->
<script src="../../resource/assets3/owl-carousel/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-brand").owlCarousel({
            autoPlay: 3000,
            items : 1,
            itemsDesktop : [1199,1],
            itemsDesktopSmall : [979,2],
            navigation: false,
        });
    });
</script>
</body>
</html>

