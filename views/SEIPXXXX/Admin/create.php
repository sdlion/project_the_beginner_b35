<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('login_page.php');
    return;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Doctor info</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets1/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets1/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets1/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets1/css/style.css">


    <script src="../../../resource/assets1/js/html5shiv.js"></script>
    <script src="../../../resource/assets1/js/respond.min.js"></script>
    <script type="text/javascript" src="../../../resource/assets1/bootstrap/js/jquery.js"></script>

    <![endif]-->
</head>

<body">

<!-- Top content -->
<div class="top-content">
    <div class="container">
        <div class="row">
            <a href='index.php'><button class='btn btn-success'>List of doctor info </button></a>
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Create Doctor </h3>
                        <p></p>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="store.php" method="post" class="login-form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="" for="name">Doctor Name :</label>
                            <input type="text" name="doctor_name" placeholder="doctor_name..." class="form-name form-control" id="form-doctor_name">
                        </div>
                        <div class="form-group">
                            <label class="" for="email" >Email:</label>
                            <input type="email" name="email" placeholder="email..." class="form-control" id="">

                        </div>

                        <div class="form-group">
                            <label class="" for="name">doctor_details:</label>
                            <input type="text" name="doctor_details" placeholder="doctor_details.." class="form-name form-control" id="form-password">
                        </div>
                        <div class="form-group">
                            <label class="" for="catagory">Catagory</label>
                            <select name="catagory" class="form-control">
                                <option   value="">..Select Catagory..</option>
                                <option   value="Dentist">Dentist</option>
                                <option  value="Cardiologist">Cardiology</option>
                                <option  value="Bone">Bone</option>
                                <option  value="Heart">Medicine</option>
                                <option  value="Surgeons">Surgeons</option>
                                <option  value="Psychiatrists">Psychiatrists</option>
                                <option  value="Kidney">Kidney</option>
                                <option  value="General Physician">General Physician</option>
                                <option  value="Gynecologist">Gynecology</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="" for="name">Phone :</label>
                            <input type="text" name="phone" placeholder="phone..." class="form-name form-control" id="form-phone">
                        </div>

                        <div class="form-group">
                            <label class="" for="address">Address:</label>
                            <input type="text" name="address" placeholder="address..." class="form-name form-control" id="form-address">
                        </div>

                        <div class="form-group">
                            <label class="" for="time_duration">time_duration:</label>
                            <input type="text" name="time_duration" placeholder="time_duration..." class="form-name form-control" id="form-address">
                        </div>

                        <div class="form-group">
                            <label class=""> Select image to Upload:</label>
                            <input type ="file" name="image" id="image"><br>
                            </div>
                        <div class="form-group">
                            <label class="" for="appointTimeStartHour">appointTimeStartHour:</label>
                            <input type="text" name="appointTimeStartHour" placeholder="appointTimeStartHour..." class="form-name form-control" id="form-address">
                        </div>
                        <div class="form-group">
                            <label class="" for="appointTimeStartMunute">appointTimeStartMunute:</label>
                            <input type="text" name="appointTimeStartMunute" placeholder="appointTimeStartMunute..." class="form-name form-control" id="form-address">
                        </div>
                        <div class="form-group">
                            <label class="" for="appointTimeEndHour">appointTimeEndHour:</label>
                            <input type="text" name="appointTimeEndHour" placeholder="appointTimeEndHour..." class="form-name form-control" id="form-address">
                        </div>
                        <div class="form-group">
                            <label class="" for="appointTimeEndMunute">appointTimeEndMunute:</label>
                            <input type="text" name="appointTimeEndMunute" placeholder="appointTimeEndMunute..." class="form-name form-control" id="form-address">
                        </div>
                        <button type="submit" class="btn">Create !</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets1/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets1/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets1/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets1/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets1/js/placeholder.js"></script>
<![endif]-->
</body>

</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>