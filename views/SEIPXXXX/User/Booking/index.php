<head>
    <link rel="stylesheet" href="../../../../resource/assets1/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../resource/assets1/font-awesome/css/font-awesome.min.css">
    <script src="../../../../resource/assets1/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../../resource/assets1/js/jquery.backstretch.min.js"></script>
    <style>
        table{
            text-align: center;
            margin :50px auto;
            background-color:rgba(183, 179, 179, 0.15);
        }
        th{
            text-align: center;
            height: 32px;
            color: #5e90c3;
            font-family: serif;
            font-size: 19px;
        }
        td,tr{
            padding: 20px;
            text-align: center;
        }
    </style>
</head>
<a href='../../index.php' style="margin-left:1100px;"><button class='btn btn-success'>Back</button></a>

<?php
session_start();
require_once("../../../../vendor/autoload.php");
use App\Booking\Booking;
use App\Message\Message;
use App\Utility\Utility;

$obj= new Booking();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$serial=1;
echo "<table>";
echo "<th> Serial No</th><th > ID</th><th>Patient Name</th><th>Email</th><th>Catagory</th><th>Doctor Name</th><th>Action </th>";

{
    echo "<tr style='height: 40px'>";
    echo "<td> $serial</td>";
    echo "<td> $singleUser->id</td>";
    echo "<td> $singleUser->patient_name</td>";
    echo "<td> $singleUser->email</td>";
    echo "<td> $singleUser->catagory</td>";
    echo "<td> $singleUser->doctor_name</td>";
    echo"
  <td>
        <a href='delete.php?id=$singleUser->id'onclick='return checkDelete()'><button class='btn btn-danger'><img src='../../../../resource/assets/img/backgrounds/del.png' width='15' height='15'>  Delete</button></a>

   </td>
  ";

    echo "</tr>";
    $serial++;
}
echo "</table>";
