<?php
session_start();
require_once("../../../vendor/autoload.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Appointment management system</title>


    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/assets3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../../resource/assets3/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom Theme files -->
    <link href="../../../resource/assets3/css/style.css" rel='stylesheet' type='text/css' />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../../resource/assets3/js/jquery.min.js"></script>

    <!-- Owl Carousel Assets -->
    <link href="../../../resource/assets3/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../../../resource/assets3/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets3/js/html5shiv.js"></script>
    <script src="../../../resource/assets3/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- /////////////////////////////////////////Top -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="logo"><img src="../../../resource/assets3/images/logo.png" alt="Third slide"></a>  <!... logo of health care...!>
            </div>
            <div class="col-md-6 text-right">
                <span>Information Service:</span></br>
                <strong class="contact-phone"><i class="fa fa-phone"></i>+00088061927</strong>
            </div>
        </div>
    </div>
</header>
<!-- Header -->


<!-- /////////////////////////////////////////Navigation -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="dropdown-toggle"   href="../index.php">Home</a>
                </li>
                <li>
                    <a class="dropdown-toggle"   href="About.php">About</a>
                </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="Doctor.php">Doctors Info
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="Doctor.php">services</a></li>

                        </ul>
                    </li>
                <li>
                    <a class="dropdown-toggle"  href="Blog.php">Blogs</a>
                </li>
                <li>
                    <a class="dropdown-toggle"  href="phone.php">Contacts</a>
                </li>
                <?php if ($_SESSION['email'] != null) { ?>
                    <li><a href="Authentication/logout.php" class="page-scroll">Logout</a> </li>
                <?php } else { ?>
                    <li>
                        <a class="dropdown-toggle"  href="Profile/login_page.php">Login</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<a id='backTop'>Back To Top</a>
<!-- /Back To Top -->

<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="single-page">
    <div class="container">
        <div id="main-content">
            <div class="row">
                <article class="box-shadow">
                    <div class="art-header">
                        <h2>Sharing our Explorer’s Story</h2>
                        <div class="info">By <a href="#">Admin</a> on April 14, 2016</div>
                    </div>
                    <div class="art-content">
                        <img src="../../../resource/assets3/images/7.jpg" />
                        <div class="excerpt"><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum exercitation ullamco laboris nisi ut aliquip.</p></div>
                        <p>When you imagine inventors, you probably picture a lone genius in a laboratory concocting brilliant devices, experimenting and redesigning until some concept or contraption works perfectly. At that point, the new invention is unveiled to the world, a stunning piece of new technology that instantly changes everything.

                            Well, you've got part of it right. There's certainly a lot of redesigning and experimenting when it comes to inventions, but it takes a lot longer than you think. It also takes far more people than that lone genius.

                            As you'll see when you read about these 10 world-changing inventions, no invention is created in a vacuum. Every single one was built on previous inventions created by other inventors years, decades or even centuries before. Every invention has problems, and it might not be until some other inventor comes along that they get solved. To confuse things further, it usually isn't the original inventor who gets all the credit, but rather the inventor who made the one crucial improvement that makes us all want one.</p>
                        <h2>Cardiology </h2>
                        <p>Cardiology (from Greek καρδίᾱ kardiā, "heart" and -λογία -logia, "study") is a branch of medicine dealing with disorders of the heart as well as parts of the circulatory system. The field includes medical diagnosis and treatment of congenital heart defects, coronary artery disease, heart failure, valvular heart disease and electrophysiology. Physicians who specialize in this field of medicine are called cardiologists, a specialty of internal medicine. Pediatric cardiologists are pediatricians who specialize in cardiology. Physicians who specialize in cardiac surgery are called cardiothoracic surgeons or cardiac surgeons, a specialty of general surgery.</p> <p>A clinical cardiologist is an internal physician who specializes in diagnosing and treating conditions that affect the heart and surrounding blood vessels. Cardiologists require specialized education and training that teaches them how to prevent and treat diseases affecting the heart.</p>
                        <p>The study of the electrical aspects is a sub-field of electrophysiology called cardiac electrophysiology and is epitomized with the electrocardiogram (ECG/EKG). The action potentials generated in the pacemaker propagate throughout the heart in a specific pattern.</p>
                        <h2>Medicine</h2>
                        <p>Medicine (British English Listeni/ˈmɛdsᵻn/; American English Listeni/ˈmɛdᵻsᵻn/) is the science and practice of the diagnosis, treatment, and prevention of disease. The word medicine is derived from Latin medicus, meaning "a physician". Medicine encompasses a variety of health care practices evolved to maintain and restore health by the prevention and treatment of illness. Contemporary medicine applies biomedical sciences, biomedical research, genetics, and medical technology to diagnose, treat, and prevent injury and disease, typically through pharmaceuticals or surgery, but also through therapies as diverse as psychotherapy, external splints and traction, medical devices, biologics, and ionizing radiation, amongst others.</p>
                        <p><h6>Types of medicines</h6>

                        Most medicines come in a variety of types or formats. Be aware, though, that some medicines (particularly rare or unusual ones) only come in one type. Also, some may be more effective in one type than another.
                        Preparations
                        In the UK, medicines often come in some of the following preparations:</p>

                        <p>Liquid:</p>
                        The active part of the medicine is combined with a liquid to make it easier to take or better absorbed. A liquid may also be called a ‘mixture’, ‘solution’ or ‘syrup’. Many common liquids are now available without any added colouring or sugar.
                        </p>
                        <p> Tablet:</p>
                        The active ingredient is combined with another substance and pressed into a round or oval solid shape. There are different types of tablet. Soluble or dispersible tablets can safely be dissolved in water.
                        </p>
                        <p>   Capsules:</P>
                        The active part of the medicine is contained inside a plastic shell that dissolves slowly in the stomach. Some capsules can be taken apart so the contents can be mixed with a favourite food. Others need to be swallowed whole so the medicine is not absorbed until the stomach acid breaks down the capsule shell.</p>
                        <h2>Gynaecology</h2>
                        <p>Gynaecology or gynecology (see spelling differences) is the medical practice dealing with the health of the female reproductive systems (vagina, uterus, and ovaries) and the breasts. Literally, outside medicine, the term means "the science of women". Its counterpart is andrology, which deals with medical issues specific to the male reproductive system.</p>
                        <p>The Kahun Gynaecological Papyrus is the oldest known medical text of any kind. Dated to about 1800 BC, it deals with women's complaints—gynaecological diseases, fertility, pregnancy, contraception, etc. The text is divided into thirty-four sections, each section dealing with a specific problem and containing diagnosis and treatment; no prognosis is suggested. Treatments are non surgical, comprising applying medicines to the affected body part or swallowing them. The womb is at times seen as the source of complaints manifesting themselves in other body parts.[1]

                            The Hippocratic Corpus contains several gynaecological treatises dating to the 5th/4th centuries BC. Aristotle is another strong source for medical texts from 4th century BC with his descriptions of biology primarily found in History of Animals, Parts of Animals, Generation of Animals. The gynaecological treatise Gynaikeia by Soranus of Ephesus (1st/2nd century AD) is extant (together with a 6th-century Latin paraphrase by Muscio, a physician of the same school). He was the chief representative of the school of physicians known as the "Methodists".

                            J. Marion Sims is widely considered the father of modern gynaecology. Now criticized for the short comings. He developed some of his techniques by operating on slaves, many of whom were not given anaesthesia.</p>

                        <div class="note">

                            <div class="clear"></div>
                        </div>

                    </div>
                </article>
            </div>
        </div>
    </div>

</div>

<footer>
    <div class="wrap-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-footer footer-1">
                    <div class="footer-heading"><h4>Partners</h4></div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/22.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/23.jpg" /></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/24.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/18.jpg" /></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/19.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/20.jpg" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-2">
                    <div class="footer-heading"><h4>About Us</h4></div>
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad</p>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-3">
                    <div class="footer-heading"><h4>Follow us</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a></li>
                            <li><a href="#"><i class="fa fa-rss"></i> RSS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-4">
                    <div class="footer-heading"><h4>Navigation</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="../index.php"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="About.php"><i class="fa fa-users"></i> About</a></li>
                            <li><a href="Doctor.php"><i class="fa fa-ambulance"></i> Doctors Info</a></li>
                            <li><a href="Blog.php"><i class="fa fa-folder-open-o"></i>  Blogs</a></li>
                            <li><a href="phone.php"><i class="fa fa-envelope-o"></i> Contacts</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="coppy-right">
    <div class="wrap-footer">
        <div class="clearfix">
            <div class="col-md-6 col-md-offset-3">
                <p>Copyright @ 2016 The Begginners</p>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->

<!-- Core JavaScript Files -->
<script src="../../../resource/assets3/js/bootstrap.min.js"></script>
<script src="../../../resource/assets3/js/jquery.backTop.min.js"></script>
<script>
    $(document).ready( function() {
        $('#backTop').backTop({
            'position' : 1200,
            'speed' : 500,
            'color' : 'red',
        });
    });
</script>

</body>
</html>
	
