<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Appointment management system</title>



    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/assets3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../../resource/assets3/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom Theme files -->
    <link href="../../../resource/assets3/css/style.css" rel='stylesheet' type='text/css' />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../../resource/assets3/js/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets3/js/html5shiv.js"></script>
    <script src="../../../resource/assets3/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- /////////////////////////////////////////Top -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="logo"><img src="../../../resource/assets3/images/logo.png" alt="Third slide"></a>  <!... logo of health care...!>
            </div>
            <div class="col-md-6 text-right">
                <span>Information Service:</span></br>
                <strong class="contact-phone"><i class="fa fa-phone"></i>+00088061927</strong>
            </div>
        </div>
    </div>
</header>
<!-- Header -->

<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="archives-page">
    <div class="container">
        <div class="row">
            <div id="main-content">
                <article class="box-shadow">




                    <div class="art-content">


                        <p>Dr. Jindal is a member of several professional associations such as the American society for Reproductive Medicine, Indian Society for Assisted Reproduction, Indian society of Perinatology and Reproductive Biology, Federation of Obstetrics and Gynaecological societies of India, Indian Fertility Society, National association of Voluntary Sterilization and Family Planning of India and Indian Medical Association. She is the founder of Chandigarh Obstetrics and Gynaecology Society.
                            Dr. Jindal has got several research papers published in national and international journals. She has obtained additional training in IVF and ICSI from Institute of Reproductive Medicine Kolkata and Reproductive Science Associates of Kansas city, USA under auspicious of Integra Medical, America.
                            She is a pioneer and has over 25 years of experience in the field of infertility. She was the first one to start specialized infertility services in Chandigarh including IVF and ICSI.</p>

                    </div>
                    <a class="btn btn-lg btn-2 box-shadow" href="Blog.php">Back</a>
                </article>
            </div>
        </div>
    </div>

</div>
