<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Appointment management system</title>



    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/assets3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../../resource/assets3/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom Theme files -->
    <link href="../../../resource/assets3/css/style.css" rel='stylesheet' type='text/css' />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../../resource/assets3/js/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets3/js/html5shiv.js"></script>
    <script src="../../../resource/assets3/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- /////////////////////////////////////////Top -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="logo"><img src="../../../resource/assets3/images/logo.png" alt="Third slide"></a>  <!... logo of health care...!>
            </div>
            <div class="col-md-6 text-right">
                <span>Information Service:</span></br>
                <strong class="contact-phone"><i class="fa fa-phone"></i>+00088061927</strong>
            </div>
        </div>
    </div>
</header>
<!-- Header -->

<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="archives-page">
    <div class="container">
        <div class="row">
            <div id="main-content">
                <article class="box-shadow">




<div class="art-content">

    <p>
        He returned to India in 1989 and initially worked at B.M. Birla Hospital in Kolkata.
        He performed the first neonatal heart surgery in the country on a 9-day-old baby named "Ronnie" 1992 a successful operation in medical history. In Kolkata he operated on Mother Teresa after she had a heart attack and subsequently served as her personal physician. After some time, he moved to Bangalore and started the Manipal Heart Foundation at Manipal Hospitals, Bangalore. Financial contribution for the construction of the hospital was provided by Shetty's father-in-law. He was elected as the chief patron of Indian Association of Clinical Cardiologists during the annual scientific session IACCCON 2013 at Bangalore.

        In 2001, Shetty foun
        ded Narayana Hrudayalaya (NH), a multi-specialty hospital in Bommasandra on the outskirts of Bangalore. He believes that the cost of healthcare can be reduced by 50 percent in the next 5–10 years if hospitals adopt the idea of economies of scale. Apart from cardiac surgery, NH also has cardiology, neurosurgery, paediatric surgery, haematology and transplant services, and nephrology among various others. The heart hospital is the largest in the world with 1000 beds performing over 30 major heart surgeries a day. The land on which the health city was built, was previously a marshland which was reclaimed for this purpose. The Health City intends to cater to about 15,000 outpatients every day. In August 2012, Shetty announced an agreement with TriMedx, a subsidiary of Ascension Health, to create a joint venture for a chain of hospitals in India. In the past Narayana Hrudayalaya has collaborated with Ascension Health to set up a health care city in Cayman Islands, planned to eventually have 2,000 beds.[9]

        Shetty als
        o founded Rabindranath Tagore International Institute of Cardiac Sciences (RTIICS) in Kolkata, and signed a memorand
        um of understanding with the Karnataka Government to build 5,000 bed speciality hospital near Bangalore International Airport. His company signed a MOU with the Government of Gujarat, to set up a 5,000 bed hospital at Ahmedabad.

        He was a pa
        rt of the seven-member panel of Board of Governors which replaced the MCI and served for a period of one year before it was
        further reconstituted.


</div>
<a class="btn btn-lg btn-2 box-shadow" href="Blog.php">Back</a>
                </article>
            </div>
        </div>
    </div>

</div>
