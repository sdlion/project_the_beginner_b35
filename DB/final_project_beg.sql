-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 02, 2016 at 04:22 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `final_project_beg`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'jerinisrat@gmail.com', '1234'),
(2, 'israt@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `catagory` varchar(111) NOT NULL,
  `doctor_name` varchar(111) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `patient_name`, `email`, `catagory`, `doctor_name`, `date`) VALUES
(6, 'zvzz', '', 'Surgeons', 'Professor Dr. A.K.M. Anwarul Islam', '0000-00-00'),
(7, 'ema', '', 'Cardiologist', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(8, 'hhh', 'jerinisrat.cse@gmail.com', 'Cardiologist', 'Professor Dr. A. B. M. Yunus', '0000-00-00'),
(9, 'hhh', 'jerinisrat.cse@gmail.com', 'Bone', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(10, 'hhh', 'israt.ctg75@gmail.com', 'Dentist', 'Professor Dr. A. B. M. Yunus', '0000-00-00'),
(11, 'vcb', 'jerinisrat.cse@gmail.com', 'Bone', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(12, 'jjjj', 'jerinisrat.cse@gmail.com', 'Cardiologist', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(16, 'rita', 'antikasaha1994@gmail.com', 'Cardiologist', 'Dr. Pradeep k. Shetty', '0000-00-00'),
(17, 'hasan', 'hasan@yahoo.com', 'Surgeons', 'Dr. Fuad Nazim', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
  `id` int(111) NOT NULL AUTO_INCREMENT,
  `doctor_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `doctor_details` text NOT NULL,
  `catagory` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` text NOT NULL,
  `time_duration` varchar(111) NOT NULL,
  `image` varchar(111) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No',
  `appointTimeStartHour` int(11) NOT NULL,
  `appointTimeStartMunute` int(11) NOT NULL,
  `appointTimeEndHour` int(11) NOT NULL,
  `appointTimeEndMunute` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `doctor_name`, `email`, `doctor_details`, `catagory`, `phone`, `address`, `time_duration`, `image`, `is_deleted`, `appointTimeStartHour`, `appointTimeStartMunute`, `appointTimeEndHour`, `appointTimeEndMunute`) VALUES
(6, 'Dr. Pradeep k. Shetty', 'pradeep@gmail.com', 'M.D. , D.M. FRCP ', 'Cardiologist', '018547896', 'Golpahar, chittagong', '5pm-7pm', '1480604498images.jpg', 'No', 8, 30, 9, 0),
(7, 'Dr. Bijay kumar ', 'kumar@gmail.com', 'senior consultant', 'General Physician', '017156984', 'chittagong medical', '7pm-9pm', '1480604587images2.jpg', 'No', 9, 30, 10, 0),
(8, 'Dr. Shofiur Rahaman ', 'rahaman86@gmail.com', 'FRCS(glasg)', 'Bone', '019945846', 'GEC circle chittagong', '8pm-10pm', '1480604691images3.jpg', 'No', 8, 0, 8, 30),
(9, 'DR. Alam Jaman', 'alam1990@yahoo.com', 'M.S. , M.Ch. , DNB', 'Surgeons', '0185236987', 'chittagong medical', '7pm-10pm', '1480604754images8.jpg', 'No', 8, 30, 9, 30),
(10, 'Dr. Nafiza Zaman', 'nafiza1988@gmail.com', 'MBBS, F.R.C.S', 'Gynecologist', '018545635', 'Golpahar circle', '3pm-6pm', '1480604818images1.jpg', 'No', 9, 30, 10, 0),
(11, 'Dr. Fuad Nazim', 'fuad@yahoo.com', 'MBBS, F.C.P.S', 'Surgeons', '018564236', 'chittagong medical', '7pm-10pm', '1480640032images10.jpg', 'No', 6, 0, 7, 0),
(12, 'Dr. Fariya khan', 'faria1998@gmail.com', 'MBBS, F.R.C.S', 'Psychiatrists', '0184569874', 'foujderhat, ctg', '6pm-9pm', '1480640313images4.jpg', 'No', 10, 0, 10, 30),
(13, 'Dr. Bidhan Roy ', 'roy1982@gmail.com', 'M.S. , M.Ch. , DNB', 'General Physician', '01718569874', 'colonelhat ctg', '6pm-11pm', '1480640371images11.jpg', 'No', 10, 30, 11, 0),
(14, 'Dr. mahadi Tahamid', 'tahameed@yahoo.com', 'M.D.,FPS', 'Dentist', '018563247', 'agrabad hospital', '5pm-7pm', '1480640450images16.jpg', 'No', 11, 0, 11, 30),
(15, 'Dr.Sanjeeda keya', 'sanjeeda1981@gmail.com', '', 'Surgeons', '0171863548', 'agrabad hospital', '', '1480489613download.jpg', 'No', 0, 0, 0, 0),
(16, 'Dr. Sajjad Hossain', 'sajjad558@yahoo.com', 'M.S. , M.Ch. , DNB', '', '01716589514', 'chittagong medical', '', '1480489685download.jpg', 'No', 0, 0, 0, 0),
(17, 'Nowrin Hassan', 'noh123@gmail.com', 'MBBS, F.C.P.S', 'Gynecologist', '1478523698', 'Chittagong medical', '7pm-9pm', '1480599074images1.jpg', 'No', 0, 0, 0, 0),
(18, 'Rama Barua', 'ramabb@yahoo.com', 'MBBS,', 'Heart', '15963587412', 'CSCR,ctg', '8pm-10pm', '1480601157images4.jpg', 'No', 20, 0, 21, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(111) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(1, 'Israt', 'ema', 'israt.ctg75@gmail.com', '182be0c5cdcd5072bb1864cdee4d3d6e', '33', 'cc', 'd87391f56ebb944ce0d45cd13b7aec96'),
(2, 'Tasnia', 'xc', 'jerinisrat.cse@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '436', 'dgffdg', 'Yes'),
(3, 'test1', 'test2', 'shwetariya1611@gmail.com', '202cb962ac59075b964b07152d234b70', '3647588', 'fhgg43654 3646 ', '8ff56f513c85467703e9c61be0a24f2b'),
(4, 'test1', 'hhg', 'graphical4200@gmail.com', '202cb962ac59075b964b07152d234b70', '677', 'jhjhhj', 'Yes'),
(8, 'gggg', 'gg', 'milondas117@gmail.com', '202cb962ac59075b964b07152d234b70', '2555', 'kkkk', 'Yes'),
(14, 'anu', 'anu', 'antikasaha1994@gmail.com', '202cb962ac59075b964b07152d234b70', '123654789', 'colonelhat,ctg', '16117059ba785ae7fa6b26269c75326d');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
